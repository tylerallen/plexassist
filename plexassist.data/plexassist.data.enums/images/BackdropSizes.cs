﻿namespace plexassist.data.enums.images {
    public enum BackdropSizes {
        w300,
        w780,
        w1280,
        original
    }
}
﻿namespace plexassist.data.enums.images {
    public enum LogoSizes {
        w45,
        w92,
        w154,
        w185,
        w300,
        w500,
        original
    }
}
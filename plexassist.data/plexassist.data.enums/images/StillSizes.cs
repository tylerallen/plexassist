﻿namespace plexassist.data.enums.images {
    public enum StillSizes {
        w92,
        w185,
        w300,
        original
    }
}
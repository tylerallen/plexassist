﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThePirateBay;
using TMDbLib.Objects.Search;

namespace plexassist.data.models
{
    public class MovieSearchResult
    {

        public MovieSearchResult() { }
        public IEnumerable<SearchMovie> Movies { get; set; }
        public IEnumerable<Torrent> Torrents { get; set; }


    }
}

﻿using System.Collections.Generic;
using ThePirateBay;

namespace plexassist.data.models {
    public class MovieResult {
        public TMDbLib.Objects.Movies.Movie Movie { get; set; }
        public IEnumerable<Torrent> Torrents { get; set; }
    }
}
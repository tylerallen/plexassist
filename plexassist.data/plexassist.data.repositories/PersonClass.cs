﻿using TMDbLib.Client;
using TMDbLib.Objects.People;

namespace plexassist.data.repositories {
    public class PersonClass {
        private readonly int _personId;
        private readonly string _tmdbApiKey;

        public PersonClass(string tmdbApiKey, int personId) {
            _tmdbApiKey = tmdbApiKey;
            _personId = personId;

            var client = new TMDbClient(_tmdbApiKey);
            var person = client.GetPerson(personId);
            person.Images = client.GetPersonImages(personId);
            person.Credits = client.GetPersonMovieCredits(personId);
            Person = person;
        }

        public Person Person { get; internal set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace plexassist.data.repositories
{
    public class MovieClass {
        private readonly string _tmdbApiKey;
        private readonly int _movieId;

        public MovieClass(string tmdbApiKey, int movieId) {
            _tmdbApiKey = tmdbApiKey;
            _movieId = movieId;

            var client = new TMDbLib.Client.TMDbClient(_tmdbApiKey);
            var movie = client.GetMovie(_movieId);
            movie.Videos = client.GetMovieVideos(movieId);
            movie.Credits = client.GetMovieCredits(movieId);
            Movie = movie;
        }

        public TMDbLib.Objects.Movies.Movie Movie { get; internal set; }
    }
}

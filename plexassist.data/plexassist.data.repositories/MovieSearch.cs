﻿using System.Collections.Generic;
using System.Linq;
using TMDbLib.Client;
using TMDbLib.Objects.Search;

namespace plexassist.data.repositories {
    public class MovieSearch {
        private readonly string _tmdbApiKey;

        public MovieSearch(string tmdbApiKey) {
            _tmdbApiKey = tmdbApiKey;
        }

        public List<SearchMovie> Query(string query) {
            var client = new TMDbClient(_tmdbApiKey);
            var results = client.SearchMovie(query, "en");
            var output = results.Results.Where(x => !string.IsNullOrEmpty(x.PosterPath));
            //.OrderByDescending(x => x.Popularity)
            //.ThenBy(x => x.ReleaseDate).ToList();
            return output.ToList();
        }
    }
}
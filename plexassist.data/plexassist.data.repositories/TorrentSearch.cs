﻿using System.Collections.Generic;
using System.Linq;
using ThePirateBay;

namespace plexassist.data.repositories {
    public class TorrentSearch {
        public List<Torrent> Query(string query) {
            return queryPirateBay(query);
        }

        private List<Torrent> queryPirateBay(string query) {
            var tpbQuery = new Query(query);
            var tpbResults = Tpb.Search(tpbQuery);
            return tpbResults.ToList();
        }
    }
}
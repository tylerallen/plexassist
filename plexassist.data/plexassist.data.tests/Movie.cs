﻿using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace plexassist.data.tests {
    [TestClass]
    public class Movie {
        private string _apiKey;

        [TestInitialize]
        public void testInitialize() {
            _apiKey = ConfigurationManager.AppSettings["tmdb.apikey"];
        }


        [TestMethod]
        public void query_tmdb_for_single_movie() {
            var search = new repositories.MovieSearch(_apiKey);
            var result = search.Query("fight club");

            var movieTest = result.First();

            var movieClass = new repositories.MovieClass(_apiKey, movieTest.Id);
            Assert.IsNotNull(movieClass);
            Assert.IsNotNull(movieClass.Movie);
            Assert.IsTrue(movieClass.Movie.Title == "Fight Club");
        }
    }
}
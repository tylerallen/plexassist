﻿using System;
using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace plexassist.data.tests
{
    [TestClass]
    public class MovieSearch {
        private string _apiKey;
        [TestInitialize]
        public void testInitialize() {
            _apiKey = ConfigurationManager.AppSettings["tmdb.apikey"];
        }
        [TestMethod]
        public void query_tmdb_for_movies()
        {
            var search = new repositories.MovieSearch(_apiKey);
            var result = search.Query("fight club");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());

        }

        
    }
}

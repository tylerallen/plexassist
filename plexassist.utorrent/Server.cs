﻿using System;

namespace plexassist.utorrent {
    public class Server {
        public Uri Host { get; set; }
        public int Port { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
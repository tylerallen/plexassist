﻿namespace plexassist.web.Constants
{
    public static class HomeControllerRoute
    {
        public const string GetAbout = ControllerName.Home + "GetAbout";
        public const string GetBrowserConfigXml = ControllerName.Home + "GetBrowserConfigXml";
        public const string GetContact = ControllerName.Home + "GetContact";
        public const string GetFeed = ControllerName.Home + "GetFeed";
        public const string GetIndex = ControllerName.Home + "GetIndex";
        public const string GetManifestJson = ControllerName.Home + "GetManifestJson";
        public const string GetOpenSearchXml = ControllerName.Home + "GetOpenSearchXml";
        public const string GetRobotsText = ControllerName.Home + "GetRobotsText";
        public const string GetSearch = ControllerName.Home + "GetSearch";
        public const string GetMovie = ControllerName.Home + "GetMovie";
        public const string GetPerson = ControllerName.Home + "GetPerson";
        public const string GetSitemapXml = ControllerName.Home + "GetSitemapXml";
    }
}
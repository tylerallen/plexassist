﻿namespace plexassist.web.Constants
{
    public class Application
    {
        public const string Name = "Plex Assist";
        public const string ShortName = "Plex Assist";
    }
}